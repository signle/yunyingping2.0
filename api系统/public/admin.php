<?php
namespace think;
header('Content-Type:text/html;charset=utf-8');
require __DIR__ . '/../library/Base.php';
require __DIR__ . '/../thinkphp/base.php';
define('APP_PATH', __DIR__ . '/application/');
define('DS', DIRECTORY_SEPARATOR);
define('ENTRANCE', 'admin');
if(!is_file('./../install.lock')) {
    header('location: /');
} else {
    Container::get('app')->run()->send();
}
