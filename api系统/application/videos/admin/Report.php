<?php
namespace app\videos\admin;
use app\system\admin\Admin;
use app\videos\model\Report as ReportModel;

class Report extends Admin
{
    protected $oneModel = 'Report';//模型名称[通用添加、修改专用]
    protected $oneTable = '';//表名称[通用添加、修改专用]
    protected $oneAddScene = '';//添加数据验证场景名
    protected $oneEditScene = '';//更新数据验证场景名

    public function initialize()
    {
        parent::initialize();
        $this->ReportModel = new ReportModel();
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            if (isset($data['source_url']) && !empty($data['source_url'])) {
                $map[] = ['source_url|source_id|source_title', 'eq', trim($data['source_url'])];
            }
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;
            $map[] = ['status', 'eq', 0];
            $data = $this->ReportModel->getList($map, $page, $limit, 'number desc, create_time desc');
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }
}