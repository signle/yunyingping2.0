<?php
namespace app\videos\api;
use app\one_api\api\UserInit;
use app\videos\server\Analysis as AnalysisServer;

class Analysis extends UserInit
{

    public function initialize() 
    {
        // 是否验证登录
        $this->check_login = false;
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->AnalysisServer = new AnalysisServer();
    }

    /**
     * 获取解析地址
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getAnalysis()
    {
        $data= $this->params;
        $result = $this->AnalysisServer->getAnalysis($data, $this->user);
        if (false === $result) {
            return $this->_error($this->AnalysisServer->getError(),'',80001);
        }
        return $this->_success("成功", $result);
    }

}