<?php
namespace app\user\admin;

use app\system\admin\Admin;
use app\user\model\UserGroup as GroupModel;

/**
 * 会员分组控制器
 * @package app\user\admin
 */
class Group extends Admin
{
    protected $oneModel = 'UserGroup';

    /**
     * 分组列表
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if ($this->request->isAjax()) {

            $data = $map = [];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;
            $data = (new GroupModel)->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);

        }

        return $this->fetch();
    }
    
    /**
     * 设置默认等级
     *
     * @param integer $id
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function setDefault($id = 0)
    {
        GroupModel::where('id', 'neq', $id)->setField('default', 0);
        
        if (GroupModel::where('id', $id)->setField('default', 1) === false) {
            return $this->error('设置失败');
        }

        return $this->success('设置成功');
    }
}
