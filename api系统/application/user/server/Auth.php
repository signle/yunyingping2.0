<?php
namespace app\user\server;
use app\common\server\Service;
use app\user\model\User;

class Auth extends Service{

    public function initialize() {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->UserModel = new User();
    }
    /**
     * 登录等统一入口
     *
     * @param [type] $data
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function dosth($data) {
        $mobile = isset($data['account']) ? $data['account'] : '';
        $password = isset($data['password']) ? $data['password'] : '';
        $code = isset($data['code']) ? $data['code'] : '';
        $remember = isset($data['remember']) ? ($data['remember'] == 1 ? true : false) : false;
        switch($data['type']){
            case 'reg':
                $return = $this->UserModel->register($data);
                break;
            case 'login':
                $return = $this->UserModel->login($mobile, $password, $code, $remember);
                break;
            case 'forget':
                $return = $this->UserModel->forget($data);
                break;
        }
        if (false === $return) {
        	$this->code = $this->UserModel->code ? $this->UserModel->code : '';
            $this->error = $this->UserModel->getError();
            return false;
        }
        return $return;
    }
}