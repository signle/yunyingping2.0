/*
 sql安装文件
*/
DROP TABLE IF EXISTS `one_banner`;
CREATE TABLE `one_banner`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '视频id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'banner标题',
  `img` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'banner图片地址',
  `isad` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0  banner 1 广告 2 小程序广告',
  `ad_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告链接',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'image' COMMENT '展示类型 image video mpad',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1 启用 0 禁用',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轮播图' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `one_ad`;
CREATE TABLE `one_ad`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 启动页广告 2 首页广告 3 播放页弹窗广告 4 播放页底部广告 5 视频播放前广告 6 播放前激励广告',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '广告标题',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '广告简介',
  `ad_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 自定义图片广告 2 自定义视频广告 3 小程序BANNER广告 4 小程序视频广告 5 小程序插屏广告 6 小程序格子广告 7 小程序视频贴片广告 8 小程序激励式广告 9 小程序原生模板广告',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片地址',
  `v_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '跳转链接',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '1 启用 0 禁用',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact COMMENT = '广告管理';
