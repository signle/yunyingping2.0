<?php
namespace app\wallpaper\server;

use app\common\server\Service;
use one\Http;

class Wallpaper extends Service
{

    public function initialize() 
    {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
    }
    
    public function getCategoryList($data){
        $_ret = cache('CategoryList');
        if (!$_ret) {
            $ret = Http::get('http://service.picasso.adesk.com/v1/vertical/category?adult=false&first=1');
            $ret = json_decode($ret, 1);
            if ($ret['code'] != 0) {
                $this->error = $ret['msg'];
                return false;
            }
            $_ret = [];
            foreach ($ret['res']['category'] as $key=>$v) {
                $_ret[$key]['name'] = $v['name'];
                $_ret[$key]['thumb'] = $v['cover'];
                $_ret[$key]['id'] = $v['id'];
            }
            cache('CategoryList', $_ret, 7200);
        }
        unset($_ret[0]);
        if ((int)config('commoncfg.show_xx') == 1) {
            unset($_ret[1]);
            unset($_ret[3]);
        }
        return array_values($_ret);
    }
    
    public function getWallpaperList($data){
        // $data['index']  分类id
        $url = 'http://service.picasso.adesk.com/v1/vertical/category/' . $data['index'] . '/vertical?limit=30&skip=' . $data['page'] . '&adult=false&first=0&order=new';
        $_ret = cache(md5($url));
        if (!$_ret) {
            $ret = Http::get($url);
            $ret = json_decode($ret, 1);
            if ($ret['code'] != 0) {
                $this->error = $ret['msg'];
                return false;
            }
            $_ret = [];
            foreach ($ret['res']['vertical'] as $key=>$v) {
                $_ret[$key]['big'] = $v['wp'];
                $_ret[$key]['thumb'] = $v['thumb'];
                $_ret[$key]['image'] = $v['img'];
                $_ret[$key]['id'] = $v['id'];
            }
            // 缓存5分钟
            cache(md5($url), $_ret, 300);
        }
        return $_ret;
    }
    
}