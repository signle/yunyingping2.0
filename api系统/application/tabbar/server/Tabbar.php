<?php
namespace app\tabbar\server;

use app\common\server\Service;
use app\tabbar\model\Tabbar as TabbarModel;

class Tabbar extends Service{

    public function initialize() {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->TabbarModel = new TabbarModel();
    }

    public function getMenus($data) {
        $map = [];
        $map[] = ['status', 'eq', 1];
        $map[] = ['tid', 'eq', 1];
        return $this->TabbarModel->getList($map, 1, 7);
    }

}