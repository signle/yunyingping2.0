<?php
namespace app\tabbar\admin;
use app\system\admin\Admin;
use app\tabbar\model\TabbarType as TabbarTypeModel;

class Type extends Admin
{

    protected $oneModel = 'TabbarType';

    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;
            $data = (new TabbarTypeModel)->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }
}