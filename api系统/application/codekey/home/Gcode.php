<?php

namespace app\codekey\home;

use app\common\controller\Common;
use app\codekey\model\UserGroupCodekey as KeyModel;
use app\user\model\User as UserModel;
use app\user\model\UserGroup as GroupModel;

class Gcode extends Common
{

    // /**
    //  * 自助获取兑换码
    //  *
    //  * @return void
    //  * @author 617 <email：723875993@qq.com>
    //  */
    // public function one()
    // {
    //     $model = new KeyModel;
    //     $user_model = new UserModel;
    //     $group_model = new GroupModel;

    //     $time = time();
    //     $ip = get_client_ip(1);
    //     // 自动处理过期兑换码
    //     $_map = [];
    //     $_map[] = ['status', 'eq', 0];
    //     $_map[] = ['exp_time', 'gt', 0];
    //     $_map[] = ['exp_time', 'lt', $time];
    //     $model->where($_map)->update(['status' => 5]);

    //     // 是否已激活
    //     $__map = [];
    //     $__map[] = ['ip', 'eq', $ip];
    //     $__map[] = ['status', 'eq', 1];
    //     // 查询当前ip被使用的卡密的用户
    //     $user_id = $model->where($__map)->value('user_id');
    //     if (!empty($user_id)) {
    //         // 查询该用户 用户组
    //         $group_id = $user_model->where('id', $user_id)->value('group_id');
    //         // 查询默认用户组
    //         $default_group_id = $group_model->where('default', '1')->value('id');
    //         // 检查该用户会员组是否不是默认的
    //         if ($group_id > $default_group_id) {
    //             echo '<html><head><meta http-equiv="Content-Type"content="text/html; charset=UTF-8"/>';
    //             echo '<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no"><title>提示</title><body>';
    //             echo "<p>您已激活成功，会员有效期内无法再次获取</p>";
    //             echo '</body></html>';
    //             die;
    //         }
    //     }

    //     // 获取频次限制 0为不限制
    //     $count = cache($ip . 'limit') ? cache($ip . 'limit') : 1;
    //     $limit = config('codekey.get_codekey_limit') > 0 ? config('codekey.get_codekey_limit') : 0;
    //     if ($limit > 0 && $count > $limit) {
    //         echo '<html><head><meta http-equiv="Content-Type"content="text/html; charset=UTF-8"/>';
    //         echo '<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no"><title>提示</title><body>';
    //         echo "<p>一天内只能获取2次</p>";
    //         echo '</body></html>';
    //         die;
    //     }

    //     $codekey = cache($ip);
    //     $exp_time = config('codekey.codekey_exp') > 0 ? $time + (config('codekey.codekey_exp') * 60) : 0;
        
    //     if (!$codekey) {
    //         // 查询是否有可用兑换码
    //         $map = [];
    //         $map[] = ['status', 'eq', 0];
    //         $map[] = ['code_type', 'eq', 1];
    //         $map[] = ['exp_time', 'gt', $time];
    //         $map[] = ['ip', 'eq', $ip];
    //         $codekey = $model->where($map)->value('codekey');
    //         if (!$codekey) {
    //             // 没有  新生成一个
    //             $codekey = substr(md5(random(16, 0) . $time), 3, 12);
    //             $data = [
    //                 'group_id' => config('commoncfg.member_group_limit'),
    //                 'codekey' => $codekey,
    //                 'exp_time' => $exp_time,
    //                 'ip' => $ip
    //             ];
    //             $model->save($data);
    //             cache($ip . 'limit', $count + 1);
    //         }
    //         cache($ip, $codekey, 180);
    //     }

    //     echo '<html><head><meta http-equiv="Content-Type"content="text/html; charset=UTF-8"/>';
    //     echo '<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no"><title>兑换码</title><body>';
    //     echo "<p>您的兑换码为：$codekey </p><p>有效期：" . ($exp_time > 0 ? config('codekey.codekey_exp') . '分钟，请尽快使用，过期无效。' : '无限制') . "</p><p style=\"color:red\">一天内只能获取2个，会员组有效期不会叠加</p>";
    //     echo '</body></html>';
    //     die;
    // }

    /**
     * 定时任务
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function task()
    {
        $model = new KeyModel;
        $time = time();
        // 自动处理过期兑换码
        $_map = [];
        $_map[] = ['status', 'eq', 0];
        $_map[] = ['exp_time', 'gt', 0];
        $_map[] = ['exp_time', 'lt', $time];
        $model->where($_map)->update(['status' => 5]);

        // 清空所有获取限制
        $info = $model->where('user_id = 0')->select()->toArray();
        foreach ($info as $key => $value) {
        	$uuid = md5($value['ip'] . $value['device_id'] . $value['model']);
            cache($uuid . 'limit', null);
        }
        echo "执行完毕\n";
    }
}
