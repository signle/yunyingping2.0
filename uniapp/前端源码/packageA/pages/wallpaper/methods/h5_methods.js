const MyApp = getApp();
const globalData = MyApp.globalData;
// 小程序激励广告
let rewardedVideoAd = null;
export default {
    data() {
        return {
            typelist: globalData.typelist,
            imageSrc: '',
            loadingBoxText: globalData.$t('common').loading,
            mainScrollHeight: 0
        };
    },
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    onLoad(params) {
        this.imageSrc = decodeURIComponent(params.url);
    },
    onShow() {
        let _this = this;
        _this.tabIndex = uni.getStorageSync('tabIndex') || 0;
    },
    onReady() {
        this.initPage();
    },
    methods: {
        initPage() {
            let _this = this;
            uni.getSystemInfo({
                success: res => {
                    let query = uni.createSelectorQuery().in(_this);
                    query.select('#navbar').boundingClientRect();
                    query.exec(re => {
                        let navbarHeight = re[0].height;
                        _this.mainScrollHeight = res.windowHeight - navbarHeight;
                    });
                }
            });
        },
        downloadImg() {
            let _this = this;
            _this.loadingBoxText = globalData.$t('common').downloading;
            MyApp.showLoading(_this.$refs.loading);
            uni.downloadFile({
                url: _this.imageSrc,
                success: res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.statusCode === 200) {
                        uni.saveImageToPhotosAlbum({
                            filePath: res.tempFilePath,
                            success: function() {
                                uni.showToast({
                                    title: '已保存到相册',
                                    icon: 'none'
                                });
                            },
                            fail: function() {
                                uni.showToast({
                                    title: '保存失败， 请稍后重试',
                                    icon: 'none'
                                });
                            }
                        });
                    } else {
                        console.log(res);
                    }
                }
            });
        },
    }
};