const MyApp = getApp();
const globalData = MyApp.globalData;
import util from '@/utils/utils.js';
export default {
    data() {
        return {
            listTop: 0,
            adSwitchKg: globalData.adSwitchKg || uni.getStorageSync('sys_config').ad_switch,
            oneAd: MyApp.getOneAd('common_ad'),
            tabIndex: 0,
            tarbarObj: globalData.tarbarObj,
            typelist: globalData.typelist,
            scrollViewHeight: 0,
            // 因为内部的滑动机制限制，请将tabs组件和swiper组件的current用不同变量赋值
            current: 0, // tabs组件的current值，表示当前活动的tab选项
            swiperCurrent: 0, // swiper组件的current值，表示当前那个swiper-item是活动的

            imageUrl: this.$config.imageUrl,
            netError: false,
            waringText: '正在加载...',
            downOption: {
                auto: false //是否在初始化后,自动执行下拉回调callback; 默认true
            },
            videosTypeList: [],
            videosMap1List: [],
            videosMap2List: [],
            videosMap3List: [],
            loading: false,
            TabCur: 0,
            TabMap1Cur: 0,
            TabMap2Cur: 0,
            TabMap3Cur: 0,
            scrollMap1Left: 0,
            scrollMap2Left: 0,
            scrollMap3Left: 0,
            videoMap: {
                type_id: '全部',
                type_id_1: '全部',
                area: '全部',
                year: '全部',
                page: 1,
                limit: 18
            },
            videoLists: [],
            videoListBoxStyle: '',
            icon: 'car',
            flowList: [],
            loadStatus: 'loadmore',
            loadText: {
                loadmore: '上拉加载更多',
                loading: '加载中，请稍候',
                nomore: '没有了哦~'
            },
            scrollTop: 0,
            topFixed: 0,
            isShowxx: false,
            navHeight: 0,
            showFill: false,
            bottomTop: 0
        };
    },
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    onPageScroll(e) {
        this.scrollTop = e.scrollTop;
    },
    onReady() {
        this.initInfo();
        this.$nextTick(function() {
            this.initScrollBox();
        });
    },
    onPullDownRefresh() {
        this.videoMap.page = 1;
        this.isEnd = false;
        this.loadStatus = 'loading';
        this.$refs.uWaterfall.clear();
        this.getVideosList();
    },
    onReachBottom() {
        this.getMore();
    },
    onShow() {
        let _this = this;
        _this.tabIndex = uni.getStorageSync('tabIndex') || 0;
        let config = uni.getStorageSync('sys_config');
        _this.isShowxx = isNaN(parseInt(config.show_xx)) ? true : parseInt(config.show_xx);
    },
    methods: {
        fixed() {
            this.showFill = true;
        },
        unfixed() {
            this.showFill = false;
        },
        initScrollBox() {
            let statusBarHeight = this.$u.sys().statusBarHeight;
            this.$u.getRect('#navbar').then(res => {
                // if (this.$isMp) {
                //     this.bottomTop = res.height + uni.upx2px(20);
                // }
                if (this.$isH5) {
                    this.topFixed = 0;
                } else {
                    this.navHeight = res.height;
                    this.topFixed = res.height + statusBarHeight;
                }
            })
        },
        tabSelect(e) {
            this.TabCur = e.currentTarget.dataset.id;
            this.videoMap.type_id = e.currentTarget.dataset.type_id;
            this.$refs.uWaterfall.clear();
            this.isEnd = false;
            this.TabMap1Cur = 0;
            this.TabMap2Cur = 0;
            this.TabMap3Cur = 0;
            this.videoMap.type_id_1 = '全部';
            this.videoMap.area = '全部';
            this.videoMap.year = '全部';
            this.videoMap.page = 1;
            this.getVideosList(true);
        },
        // 子分类
        videoMap1Select(e) {
            this.TabMap1Cur = e.currentTarget.dataset.id;
            // this.scrollMap1Left = (e.currentTarget.dataset.id - 1) * 60;
            this.videoMap.page = 1;
            this.videoMap.type_id_1 = e.currentTarget.dataset.type_id_1;
            this.$refs.uWaterfall.clear();
            this.isEnd = false;
            this.getVideosList(true);
        },
        // 地区
        videoMap2Select(e) {
            this.TabMap2Cur = e.currentTarget.dataset.id;
            // this.scrollMap1Left = (e.currentTarget.dataset.id - 1) * 60;
            this.videoMap.page = 1;
            this.videoMap.area = e.currentTarget.dataset.area;
            this.$refs.uWaterfall.clear();
            this.isEnd = false;
            this.getVideosList(true);
        },
        // 日期
        videoMap3Select(e) {
            this.TabMap3Cur = e.currentTarget.dataset.id;
            // this.scrollMap3Left = (e.currentTarget.dataset.id - 1) * 60;
            this.videoMap.page = 1;
            this.videoMap.year = e.currentTarget.dataset.year;
            this.$refs.uWaterfall.clear();
            this.isEnd = false;
            this.getVideosList(true);
        },
        goDetail(item) {
            MyApp.godetail(item.vod_id);
        },
        getVideosType() {
            let _this = this;
            _this.$http
                .post('/videos/videos/getVideoTypeLists', {})
                .then(res => {
                    _this.videosTypeList = res.data;
                    _this.videoMap.type_id = res.data[0].type_id;
                }).then(v => {
                    _this.getVideosMap1();
                })
                .catch(err => {
                    _this.waringText = '网络错误,点此重试';
                    _this.netError = true;
                });
        },
        // 子分类
        getVideosMap1() {
            let _this = this;
            _this.$http
                .post('/videos/videos/getVideoArea', {})
                .then(res => {
                    uni.setStorageSync('videosMap1', JSON.stringify(res.data));
                    _this.videosMap1List = res.data;
                }).then(v => {
                    _this.getVideosMap3();
                })
                .catch(err => {
                    _this.waringText = '网络错误,点此重试';
                    _this.netError = true;
                });
        },
        getVideosMap3() {
            let _this = this;
            _this.$http
                .post('/videos/videos/getVideoYear', {})
                .then(res => {
                    uni.setStorageSync('videosMap3', JSON.stringify(res.data));
                    _this.videosMap3List = res.data;

                }).then(v => {
                    _this.getVideosList();
                })
                .catch(err => {
                    _this.waringText = '网络错误,点此重试';
                    _this.netError = true;
                });
        },
        getMore: util.throttle(function(e) {
            this.videoMap.page = this.videoMap.page + 1;
            this.getVideosList();
        }, 800),
        initInfo() {
            this.netError = false;
            this.TabCur = 0;
            this.TabMap1Cur = 0;
            this.TabMap2Cur = 0;
            this.TabMap3Cur = 0;
            this.scrollMap1Left = 0;
            this.scrollMap2Left = 0;
            this.scrollMap3Left = 0;
            this.videoMap = {
                type_id: '全部',
                type_id_1: '全部',
                area: '全部',
                year: '全部',
                page: 1,
                limit: 10
            };
            this.getVideosType();
        },
        getVideosList() {
            var _this = this;
            if (!_this.isEnd) {
                _this.loadStatus = 'loading';
                // MyApp.showLoading(_this.$refs.loading);
                _this.$http.post('/videos/videos/getVideoLists', _this.videoMap).then(res => {
                    _this.isChange = false;
                    uni.stopPullDownRefresh();
                    // MyApp.closeLoading(_this.$refs.loading);
                    if (res.data.list.length > 0) {
                        res.data.list.forEach((v, k) => {
                            _this.flowList.push(v);
                        });
                        _this.loadStatus = 'loadmore';
                    } else {
                        _this.isEnd = true;
                        _this.loadStatus = 'nomore';
                    }
                }).then(v => {
                    this.$nextTick(() => {
                        this.$u.getRect('.do-fixed').then(rect => {
                            this.listTop = rect.height;
                        })
                    })
                }).catch(res => {
                    // MyApp.closeLoading(_this.$refs.loading);
                });
            }
        },
    }
};
