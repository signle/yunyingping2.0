const MyApp = getApp();
const globalData = MyApp.globalData;
export default {
    data() {
        return {
            adSwitchKg: globalData.adSwitchKg || uni.getStorageSync('sys_config').ad_switch,
            oneAd: MyApp.getOneAd('common_ad'),
            tabIndex: 0,
            tarbarObj: globalData.tarbarObj,
            typelist: globalData.typelist,
            loadStatus: 'loadmore',
            flowList: [],
            triggered: false,
            categoryList: [],
            categoryCurrent: 0,
            page: 0,
            isEnd: false,
            isChange: false,
            loadText: {
                loadmore: '上拉加载更多',
                loading: '加载中，请稍候',
                nomore: '没有了哦~'
            },
            scrollTop: 0
        };
    },
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    onPageScroll(e) {
        this.scrollTop = e.scrollTop;
    },
    onPullDownRefresh() {
        this.page = 0;
        this.isEnd = false;
        this.loadStatus = 'loading';
        this.getWallpaperList();
    },
    onReachBottom() {
        this.getWallpaperList();
    },
    onReady() {
        this.getWallpaperCategoryList();
    },
    onShow() {
        this.tabIndex = uni.getStorageSync('tabIndex') || 0;
        this.getWallpaperCategoryList();
    },
    methods: {
        getWallpaperCategoryList() {
            let _this = this;
            _this.loadStatus = 'loading';
            // MyApp.showLoading(_this.$refs.loading);
            _this.$http.post('/wallpaper/wallpaper/getCategoryList', {}).then(res => {
                let data = res.data;
                if (globalData.centerShowxx) {
                    data.splice(0, 1);
                    data.splice(1, 1);
                }
                _this.categoryList = data;
            }).then(v => {
                _this.getWallpaperList();
            }).catch(res => {
                MyApp.closeLoading(_this.$refs.loading);
            });
        },
        getWallpaperList() {
            let _this = this;
            if (!_this.isEnd) {
                // _this.loadStatus = 'loading';
                // MyApp.showLoading(_this.$refs.loading);
                if (_this.isChange) {
                    _this.$refs.uWaterfall.clear();
                }
                _this.$http
                    .post('/wallpaper/wallpaper/getWallpaperList', {
                        index: _this.categoryList[_this.categoryCurrent].id,
                        page: _this.page || 0
                    })
                    .then(res => {
                        _this.isChange = false;
                        uni.stopPullDownRefresh();
                        MyApp.closeLoading(_this.$refs.loading);
                        if (res.data.length > 0) {
                            res.data.forEach((v, k) => {
                                _this.flowList.push(v);
                            });
                            _this.loadStatus = 'loadmore';
                            _this.page = _this.page + 30;
                        } else {
                            _this.isEnd = true;
                            _this.loadStatus = 'nomore';
                        }
                    }).catch(res => {
                        MyApp.closeLoading(_this.$refs.loading);
                    });
            }
        },
        categoryChange(index) {
            this.categoryCurrent = index;
            this.page = 0;
            this.isEnd = false;
            this.loadStatus = 'loading';
            this.flowList = [];
            this.isChange = true;
            this.getWallpaperList();
        },
        goDetail(img) {
            if (this.$isMpWeixin) {
                uni.navigateTo({
                    url: '/packageA/pages/wallpaper/wxmp-detail?url=' + encodeURIComponent(img)
                });
            }
            if (this.$isAppPlus) {
                uni.navigateTo({
                    url: '/packageA/pages/wallpaper/app-detail?url=' + encodeURIComponent(img)
                });
            }
            if (this.$isH5) {
                uni.navigateTo({
                    url: '/packageA/pages/wallpaper/h5-detail?url=' + encodeURIComponent(img)
                });
            }
        }
    }
};
