<?php
/**
 * 模块菜单
 * 字段说明
 * url 【链接地址】格式：upgrade/控制器/方法，可填写完整外链[必须以http开头]
 * param 【扩展参数】格式：a=123&b=234555
 */
return [
    [
        'pid'           => 0,
        'is_menu'       => '1',
        'title'         => '版本控制',
        'icon'          => 'el-icon-position',
        'module'        => 'upgrade',
        'url'           => 'upgrade',
        'param'         => '',
        'target'        => '_self',
        'nav'           => 1,
        'sort'          => 100,
        'childs'         => [
            [
                'title' => '升级包管理',
                'icon' => 'fa fa-credit-card',
                'module' => 'upgrade',
                'url' => 'upgrade/upgrade_file/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
                'childs' => [
                    [
                    'title' => '编辑',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'upgrade',
                    'url' => 'upgrade/upgrade_file/edit',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                ],
            ],
            [
                'title' => '版本控制',
                'icon' => 'fa fa-credit-card',
                'module' => 'upgrade',
                'url' => 'upgrade/upgrade_version/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
                'childs' => [
                    [
                    'title' => '编辑',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'upgrade',
                    'url' => 'upgrade/upgrade_version/edit',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                ],
            ],
            [
                'title' => '授权站点',
                'icon' => 'fa fa-credit-card',
                'module' => 'upgrade',
                'url' => 'upgrade/bind/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
                'childs' => [
                     [
                    'title' => '编辑',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'upgrade',
                    'url' => 'upgrade/bind/edit',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                    [
                    'title' => '删除',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'upgrade',
                    'url' => 'upgrade/bind/del',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                ],
            ],
            [
                'title' => '异常站点',
                'icon' => 'fa fa-credit-card',
                'module' => 'upgrade',
                'url' => 'upgrade/black/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
            ],
        ]
    ],
];